export default [
  {
    XID: 1,
    DisplayImage:
      'https://robohash.org/estofficiadolorem.bmp?size=50x50&set=set1',
    BirthDate: '07/03/1984',
    Age: '36',
    Gender: 'Male',
    InsuranceProvider: 'No',
    EmployerInformation: 'Von Jearum',
    Address: '0319 American Trail',
    NID: '85-1459749',
    NIDUserType: null
  },
  {
    XID: 2,
    DisplayImage:
      'https://robohash.org/etaspernaturmodi.bmp?size=50x50&set=set1',
    BirthDate: '16/05/1998',
    Age: '22',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Berny Borgesio',
    Address: '4279 Logan Point',
    NID: '89-1072877',
    NIDUserType: null
  },
  {
    XID: 3,
    DisplayImage: 'https://robohash.org/quasisitipsum.jpg?size=50x50&set=set1',
    BirthDate: '25/05/1992',
    Age: '28',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Brenden Dumbelton',
    Address: '8155 Stang Court',
    NID: '27-3102221',
    NIDUserType: null
  },
  {
    XID: 4,
    DisplayImage: 'https://robohash.org/quaseaqueillum.png?size=50x50&set=set1',
    BirthDate: '05/10/1982',
    Age: '38',
    Gender: 'Female',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Philipa Taye',
    Address: '34 Talmadge Road',
    NID: '51-4064097',
    NIDUserType: null
  },
  {
    XID: 5,
    DisplayImage:
      'https://robohash.org/adipisciharumad.jpg?size=50x50&set=set1',
    BirthDate: '14/04/1993',
    Age: '27',
    Gender: 'Male',
    InsuranceProvider: 'No',
    EmployerInformation: 'Tracie Overland',
    Address: '113 Milwaukee Plaza',
    NID: '05-9922870',
    NIDUserType: null
  },
  {
    XID: 6,
    DisplayImage: 'https://robohash.org/maximequifugit.png?size=50x50&set=set1',
    BirthDate: '21/02/1999',
    Age: '21',
    Gender: 'Male',
    InsuranceProvider: 'No',
    EmployerInformation: 'Anselm Dorro',
    Address: '4444 Hazelcrest Center',
    NID: '07-9373468',
    NIDUserType: null
  },
  {
    XID: 7,
    DisplayImage: 'https://robohash.org/utenimaut.png?size=50x50&set=set1',
    BirthDate: '08/03/1995',
    Age: '25',
    Gender: 'Female',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Babbette Treadgear',
    Address: '81761 Ilene Lane',
    NID: '25-2718013',
    NIDUserType: null
  },
  {
    XID: 8,
    DisplayImage: 'https://robohash.org/utillumfugiat.bmp?size=50x50&set=set1',
    BirthDate: '05/09/1982',
    Age: '38',
    Gender: 'Male',
    InsuranceProvider: 'No',
    EmployerInformation: 'Alasdair McCarty',
    Address: '76307 Northland Alley',
    NID: '47-8441249',
    NIDUserType: null
  },
  {
    XID: 9,
    DisplayImage:
      'https://robohash.org/fugiatidvoluptatem.png?size=50x50&set=set1',
    BirthDate: '23/01/1981',
    Age: '39',
    Gender: 'Female',
    InsuranceProvider: 'No',
    EmployerInformation: 'Selle Gaylard',
    Address: '6383 Farwell Center',
    NID: '60-6112954',
    NIDUserType: null
  },
  {
    XID: 10,
    DisplayImage:
      'https://robohash.org/eligendivoluptatemqui.bmp?size=50x50&set=set1',
    BirthDate: '23/10/1999',
    Age: '21',
    Gender: 'Male',
    InsuranceProvider: 'No',
    EmployerInformation: 'Keen Meers',
    Address: '25 Johnson Lane',
    NID: '55-0218198',
    NIDUserType: null
  }
];
