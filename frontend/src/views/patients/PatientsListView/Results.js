import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles
} from '@material-ui/core';
import getInitials from 'src/utils/getInitials';

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2)
  }
}));

const Results = ({ className, patients, ...rest }) => {
  const classes = useStyles();
  const [selectedPatientsIds, setselectedPatientsIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleSelectAll = (event) => {
    let newselectedPatientsIds;

    if (event.target.checked) {
      newselectedPatientsIds = patients.map((patient) => patient.XID);
    } else {
      newselectedPatientsIds = [];
    }

    setselectedPatientsIds(newselectedPatientsIds);
  };

  const handleSelectOne = (event, XID) => {
    const selectedIndex = selectedPatientsIds.indexOf(XID);
    let newselectedPatientsIds = [];

    if (selectedIndex === -1) {
      newselectedPatientsIds = newselectedPatientsIds.concat(selectedPatientsIds, XID);
    } else if (selectedIndex === 0) {
      newselectedPatientsIds = newselectedPatientsIds.concat(selectedPatientsIds.slice(1));
    } else if (selectedIndex === selectedPatientsIds.length - 1) {
      newselectedPatientsIds = newselectedPatientsIds.concat(
        selectedPatientsIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newselectedPatientsIds = newselectedPatientsIds.concat(
        selectedPatientsIds.slice(0, selectedIndex),
        selectedPatientsIds.slice(selectedIndex + 1)
      );
    }

    setselectedPatientsIds(newselectedPatientsIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedPatientsIds.length === patients.length}
                    color="primary"
                    indeterminate={
                      selectedPatientsIds.length > 0
                      && selectedPatientsIds.length < patients.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell>NID</TableCell>
                <TableCell>Employer Information</TableCell>
                <TableCell>Address</TableCell>
                <TableCell>Gender</TableCell>
                <TableCell>Date of Birth</TableCell>
                <TableCell>Age</TableCell>
                <TableCell>Insurance Provider</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {patients.slice(0, limit).map((patient) => (
                <TableRow
                  hover
                  key={patient.XID}
                  selected={selectedPatientsIds.indexOf(patient.XID) !== -1}
                >
                  <TableCell padding="checkbox">
                    <Checkbox
                      checked={selectedPatientsIds.indexOf(patient.XID) !== -1}
                      onChange={(event) => handleSelectOne(event, patient.XID)}
                      value="true"
                    />
                  </TableCell>
                  <TableCell>{patient.NID}</TableCell>
                  <TableCell>
                    <Box alignItems="center" display="flex">
                      <Avatar
                        className={classes.avatar}
                        src={patient.DisplayImage}
                      >
                        {getInitials(patient.EmployerInformation)}
                      </Avatar>
                      <Typography color="textPrimary" variant="body1">
                        {patient.EmployerInformation}
                      </Typography>
                    </Box>
                  </TableCell>
                  <TableCell>{patient.Address}</TableCell>
                  <TableCell>{patient.Gender}</TableCell>
                  <TableCell>{patient.BirthDate}</TableCell>
                  <TableCell>{patient.Age}</TableCell>
                  <TableCell>{patient.InsuranceProvider}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={patients.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  patients: PropTypes.array.isRequired
};

export default Results;
