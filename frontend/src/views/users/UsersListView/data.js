export default [
  {
    XID: 1,
    DisplayImage: 'https://robohash.org/utetenim.png?size=50x50&set=set1',
    BirthDate: '08/01/1993',
    Age: '27',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Serene Shirrell',
    Address: '072 Little Fleur Hill',
    NID: '75-8615509',
    UserType: 'Doctors'
  },
  {
    XID: 2,
    DisplayImage: 'https://robohash.org/nemononsint.png?size=50x50&set=set1',
    BirthDate: '20/03/1982',
    Age: '38',
    Gender: 'Male',
    InsuranceProvider: 'No',
    EmployerInformation: 'Adah Baudone',
    Address: '76 David Court',
    NID: '57-0491035',
    UserType: 'Patient'
  },
  {
    XID: 3,
    DisplayImage:
      'https://robohash.org/omnisaliasprovident.jpg?size=50x50&set=set1',
    BirthDate: '25/02/1980',
    Age: '40',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Arlina Vince',
    Address: '55220 Pepper Wood Center',
    NID: '72-1225928',
    UserType: 'Doctors'
  },
  {
    XID: 4,
    DisplayImage:
      'https://robohash.org/teneturaspernatureos.bmp?size=50x50&set=set1',
    BirthDate: '18/08/1993',
    Age: '27',
    Gender: 'Male',
    InsuranceProvider: 'No',
    EmployerInformation: 'Franzen Ridoutt',
    Address: '07551 American Ash Alley',
    NID: '85-5627598',
    UserType: 'Doctors'
  },
  {
    XID: 5,
    DisplayImage:
      'https://robohash.org/istetemporeducimus.jpg?size=50x50&set=set1',
    BirthDate: '10/03/1988',
    Age: '32',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Gerardo Witchell',
    Address: '4786 Mcbride Street',
    NID: '21-5157636',
    UserType: 'Patient'
  },
  {
    XID: 6,
    DisplayImage:
      'https://robohash.org/possimusdictaquod.bmp?size=50x50&set=set1',
    BirthDate: '20/06/1992',
    Age: '28',
    Gender: 'Female',
    InsuranceProvider: 'No',
    EmployerInformation: 'Madella Igonet',
    Address: '8 Morrow Crossing',
    NID: '45-4857678',
    UserType: 'Doctors'
  },
  {
    XID: 7,
    DisplayImage:
      'https://robohash.org/temporareprehenderitdolorum.png?size=50x50&set=set1',
    BirthDate: '14/11/1988',
    Age: '32',
    Gender: 'Female',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Packston Hrycek',
    Address: '07 John Wall Drive',
    NID: '80-8218662',
    UserType: 'Patient'
  },
  {
    XID: 8,
    DisplayImage: 'https://robohash.org/suntinet.bmp?size=50x50&set=set1',
    BirthDate: '05/03/1988',
    Age: '32',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Abram Dumberell',
    Address: '482 Jay Road',
    NID: '81-6867492',
    UserType: 'Patient'
  },
  {
    XID: 9,
    DisplayImage:
      'https://robohash.org/liberoconsequaturomnis.jpg?size=50x50&set=set1',
    BirthDate: '06/01/1997',
    Age: '23',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Sully Spurritt',
    Address: '13 Doe Crossing Trail',
    NID: '15-7093665',
    UserType: 'Doctors'
  },
  {
    XID: 10,
    DisplayImage:
      'https://robohash.org/voluptashicimpedit.bmp?size=50x50&set=set1',
    BirthDate: '14/04/1997',
    Age: '23',
    Gender: 'Female',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Binni Poulter',
    Address: '71304 Commercial Circle',
    NID: '39-9118888',
    UserType: 'Doctors'
  }
];
