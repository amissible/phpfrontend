import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles
} from '@material-ui/core';
import getInitials from 'src/utils/getInitials';

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2)
  }
}));

const Results = ({ className, users, ...rest }) => {
  const classes = useStyles();
  const [selectedUserIds, setselectedUserIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleSelectAll = (event) => {
    let newselectedUserIds;

    if (event.target.checked) {
      newselectedUserIds = users.map((user) => user.XID);
    } else {
      newselectedUserIds = [];
    }

    setselectedUserIds(newselectedUserIds);
  };

  const handleSelectOne = (event, XID) => {
    const selectedIndex = selectedUserIds.indexOf(XID);
    let newselectedUserIds = [];

    if (selectedIndex === -1) {
      newselectedUserIds = newselectedUserIds.concat(selectedUserIds, XID);
    } else if (selectedIndex === 0) {
      newselectedUserIds = newselectedUserIds.concat(selectedUserIds.slice(1));
    } else if (selectedIndex === selectedUserIds.length - 1) {
      newselectedUserIds = newselectedUserIds.concat(
        selectedUserIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newselectedUserIds = newselectedUserIds.concat(
        selectedUserIds.slice(0, selectedIndex),
        selectedUserIds.slice(selectedIndex + 1)
      );
    }

    setselectedUserIds(newselectedUserIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedUserIds.length === users.length}
                    color="primary"
                    indeterminate={
                      selectedUserIds.length > 0
                      && selectedUserIds.length < users.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell>NID</TableCell>
                <TableCell>User Type</TableCell>
                <TableCell>Employer Information</TableCell>
                <TableCell>Address</TableCell>
                <TableCell>Gender</TableCell>
                <TableCell>Date of Birth</TableCell>
                <TableCell>Age</TableCell>
                <TableCell>Insurance Provider</TableCell>
                <TableCell>View Details</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.slice(0, limit).map((user) => (
                <TableRow
                  hover
                  key={user.XID}
                  selected={selectedUserIds.indexOf(user.XID) !== -1}
                >
                  <TableCell padding="checkbox">
                    <Checkbox
                      checked={selectedUserIds.indexOf(user.XID) !== -1}
                      onChange={(event) => handleSelectOne(event, user.XID)}
                      value="true"
                    />
                  </TableCell>
                  <TableCell>{user.NID}</TableCell>
                  <TableCell>{user.UserType}</TableCell>
                  <TableCell>
                    <Box alignItems="center" display="flex">
                      <Avatar
                        className={classes.avatar}
                        src={user.DisplayImage}
                      >
                        {getInitials(user.EmployerInformation)}
                      </Avatar>
                      <Typography color="textPrimary" variant="body1">
                        {user.EmployerInformation}
                      </Typography>
                    </Box>
                  </TableCell>
                  <TableCell>{user.Address}</TableCell>
                  <TableCell>{user.Gender}</TableCell>
                  <TableCell>{user.BirthDate}</TableCell>
                  <TableCell>{user.Age}</TableCell>
                  <TableCell>{user.InsuranceProvider}</TableCell>
                  <TableCell><Link to="/acc">View</Link></TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={users.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  users: PropTypes.array.isRequired
};

export default Results;
