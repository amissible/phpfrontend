import React from 'react';
import {
  Container,
  Grid,
  makeStyles
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import Page from 'src/components/Page';
import Profile from './Profile';
// import ProfileDetails from './ProfileDetails';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const Account = () => {
  const classes = useStyles();

  return (
    <Page
      className={classes.root}
      title="Account"
    >
      <Container maxWidth="lg">
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            lg={12}
            md={12}
            xs={12}
          >
            <Grid>
              <Link to="/users">Back</Link>
            </Grid>
          </Grid>
          <Grid
            item
            lg={12}
            md={12}
            xs={12}
          >
            <Profile />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};

export default Account;
