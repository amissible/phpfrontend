import React from 'react';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import clsx from 'clsx';
// import moment from 'moment';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  Typography,
  makeStyles
} from '@material-ui/core';

const user = {
  DisplayImage: 'https://robohash.org/utetenim.png?size=50x50&set=set1',
  BirthDate: '08/01/1993',
  Age: '27',
  Gender: 'Male',
  InsuranceProvider: 'Yes',
  EmployerInformation: 'Serene Shirrell',
  Address: '072 Little Fleur Hill',
  NID: '75-8615509',
  UserType: 'Doctors'
};

const useStyles = makeStyles(() => ({
  root: {},
  avatar: {
    height: 200,
    width: 200
  }
}));

const Profile = ({ className, ...rest }) => {
  const classes = useStyles();

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <CardContent>
        <Box>
          <Grid container spacing={4}>
            <Grid item>
              <Avatar className={classes.avatar} src={user.DisplayImage} />
            </Grid>
            <Grid
              justify="center"
              alignItems="center"
              item
              xs={12}
              sm
              container
            >
              <Grid item xs container direction="row" spacing={4}>
                <Grid item xs>
                  <Typography color="textPrimary" gutterBottom variant="h6">
                    NID
                  </Typography>
                  <Typography color="textPrimary" gutterBottom variant="h6">
                    Employer Information
                  </Typography>
                  <Typography color="textPrimary" gutterBottom variant="h6">
                    User Type
                  </Typography>
                </Grid>
                <Grid item xs>
                  <Typography
                    color="textPrimart"
                    gutterBottom
                    varient="subtitle"
                  >
                    {user.NID}
                  </Typography>
                  <Typography
                    color="textPrimary"
                    gutterBottom
                    variant="subtitle1"
                  >
                    {user.EmployerInformation}
                  </Typography>
                  <Typography
                    color="textPrimary"
                    gutterBottom
                    variant="subtitle1"
                  >
                    {user.UserType}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </CardContent>
      <Divider />
      <Divider />
      <Divider />
      <Divider />
      <CardContent>
        <Box>
          <Grid container spacing={4}>
            <Grid
              justify="center"
              alignItems="center"
              item
              xs={12}
              sm
              container
            >
              <Grid item xs container direction="row" spacing={4}>
                <Grid item xs>
                  <Typography color="textPrimary" gutterBottom variant="h6">
                    Gender
                  </Typography>
                  <Typography color="textPrimary" gutterBottom variant="h6">
                    Date of Birth (Age)
                  </Typography>
                  <Typography color="textPrimary" gutterBottom variant="h6">
                    Address
                  </Typography>
                  <Typography color="textPrimary" gutterBottom variant="h6">
                    Insurance Provider
                  </Typography>
                </Grid>
                <Grid item xs>
                  <Typography
                    color="textPrimart"
                    gutterBottom
                    varient="subtitle"
                  >
                    {user.Gender}
                  </Typography>
                  <Typography
                    color="textPrimary"
                    gutterBottom
                    variant="subtitle1"
                  >
                    {`${user.BirthDate} (${user.Age})`}
                  </Typography>
                  <Typography
                    color="textPrimary"
                    gutterBottom
                    variant="subtitle1"
                  >
                    {user.Address}
                  </Typography>
                  <Typography
                    color="textPrimary"
                    gutterBottom
                    variant="subtitle1"
                  >
                    {user.InsuranceProvider}
                  </Typography>
                </Grid>
                <Grid item xs justify="flex-end" alignItems="flex-end">
                  <CardActions>
                    <Button color="secondary" variant="text">
                      Update data
                    </Button>
                  </CardActions>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </CardContent>
    </Card>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
