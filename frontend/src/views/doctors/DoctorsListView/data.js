export default [
  {
    XID: 1,
    DisplayImage:
      'https://robohash.org/cumquesaepequod.png?size=50x50&set=set1',
    BirthDate: '13/08/1991',
    Age: '29',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Joshia Holleran',
    Address: '7 Mockingbird Park',
    NID: '81-5126467',
    NIDUserType: null
  },
  {
    XID: 2,
    DisplayImage: 'https://robohash.org/teneturetet.jpg?size=50x50&set=set1',
    BirthDate: '29/07/1992',
    Age: '28',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Trevor Ventom',
    Address: '184 Dexter Crossing',
    NID: '74-5974912',
    NIDUserType: null
  },
  {
    XID: 3,
    DisplayImage:
      'https://robohash.org/voluptatemaminima.png?size=50x50&set=set1',
    BirthDate: '27/07/1999',
    Age: '21',
    Gender: 'Female',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Lana Dorgon',
    Address: '72 Rusk Point',
    NID: '91-9766358',
    NIDUserType: null
  },
  {
    XID: 4,
    DisplayImage:
      'https://robohash.org/oditprovidentautem.bmp?size=50x50&set=set1',
    BirthDate: '06/01/1990',
    Age: '30',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Tammie Di Franceshci',
    Address: '63848 1st Way',
    NID: '77-3146964',
    NIDUserType: null
  },
  {
    XID: 5,
    DisplayImage:
      'https://robohash.org/facereestmaxime.png?size=50x50&set=set1',
    BirthDate: '11/09/1989',
    Age: '31',
    Gender: 'Male',
    InsuranceProvider: 'No',
    EmployerInformation: 'Myca Savatier',
    Address: '6 Londonderry Crossing',
    NID: '79-5108966',
    NIDUserType: null
  },
  {
    XID: 6,
    DisplayImage:
      'https://robohash.org/eiusaliquamofficia.png?size=50x50&set=set1',
    BirthDate: '27/05/1994',
    Age: '26',
    Gender: 'Female',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Nicolina Puttick',
    Address: '0 Forster Hill',
    NID: '56-2114347',
    NIDUserType: null
  },
  {
    XID: 7,
    DisplayImage:
      'https://robohash.org/autanimirepudiandae.bmp?size=50x50&set=set1',
    BirthDate: '16/09/1982',
    Age: '38',
    Gender: 'Female',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Melody Inkin',
    Address: '320 Charing Cross Avenue',
    NID: '94-4453446',
    NIDUserType: null
  },
  {
    XID: 8,
    DisplayImage:
      'https://robohash.org/impediteosbeatae.jpg?size=50x50&set=set1',
    BirthDate: '01/11/1987',
    Age: '33',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Lyman Goodlad',
    Address: '1 Esker Alley',
    NID: '11-0397602',
    NIDUserType: null
  },
  {
    XID: 9,
    DisplayImage:
      'https://robohash.org/accusamusquodrerum.bmp?size=50x50&set=set1',
    BirthDate: '21/10/1985',
    Age: '35',
    Gender: 'Male',
    InsuranceProvider: 'No',
    EmployerInformation: 'Gregory Stovin',
    Address: '6 Ohio Circle',
    NID: '85-5116625',
    NIDUserType: null
  },
  {
    XID: 10,
    DisplayImage: 'https://robohash.org/idducimusrem.png?size=50x50&set=set1',
    BirthDate: '12/12/1983',
    Age: '37',
    Gender: 'Male',
    InsuranceProvider: 'Yes',
    EmployerInformation: 'Horatio Jemison',
    Address: '1161 Green Park',
    NID: '63-7906284',
    NIDUserType: null
  }
];
