import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles
} from '@material-ui/core';
import getInitials from 'src/utils/getInitials';

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2)
  }
}));

const Results = ({ className, doctors, ...rest }) => {
  const classes = useStyles();
  const [selectedDoctorsIds, setselectedDoctorsIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleSelectAll = (event) => {
    let newselectedDoctorsIds;

    if (event.target.checked) {
      newselectedDoctorsIds = doctors.map((doctor) => doctor.XID);
    } else {
      newselectedDoctorsIds = [];
    }

    setselectedDoctorsIds(newselectedDoctorsIds);
  };

  const handleSelectOne = (event, XID) => {
    const selectedIndex = selectedDoctorsIds.indexOf(XID);
    let newselectedDoctorsIds = [];

    if (selectedIndex === -1) {
      newselectedDoctorsIds = newselectedDoctorsIds.concat(selectedDoctorsIds, XID);
    } else if (selectedIndex === 0) {
      newselectedDoctorsIds = newselectedDoctorsIds.concat(selectedDoctorsIds.slice(1));
    } else if (selectedIndex === selectedDoctorsIds.length - 1) {
      newselectedDoctorsIds = newselectedDoctorsIds.concat(
        selectedDoctorsIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newselectedDoctorsIds = newselectedDoctorsIds.concat(
        selectedDoctorsIds.slice(0, selectedIndex),
        selectedDoctorsIds.slice(selectedIndex + 1)
      );
    }

    setselectedDoctorsIds(newselectedDoctorsIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedDoctorsIds.length === doctors.length}
                    color="primary"
                    indeterminate={
                      selectedDoctorsIds.length > 0
                      && selectedDoctorsIds.length < doctors.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell>NID</TableCell>
                <TableCell>Employer Information</TableCell>
                <TableCell>Address</TableCell>
                <TableCell>Gender</TableCell>
                <TableCell>Date of Birth</TableCell>
                <TableCell>Age</TableCell>
                <TableCell>Insurance Provider</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {doctors.slice(0, limit).map((doctor) => (
                <TableRow
                  hover
                  key={doctor.XID}
                  selected={selectedDoctorsIds.indexOf(doctor.XID) !== -1}
                >
                  <TableCell padding="checkbox">
                    <Checkbox
                      checked={selectedDoctorsIds.indexOf(doctor.XID) !== -1}
                      onChange={(event) => handleSelectOne(event, doctor.XID)}
                      value="true"
                    />
                  </TableCell>
                  <TableCell>{doctor.NID}</TableCell>
                  <TableCell>
                    <Box alignItems="center" display="flex">
                      <Avatar
                        className={classes.avatar}
                        src={doctor.DisplayImage}
                      >
                        {getInitials(doctor.EmployerInformation)}
                      </Avatar>
                      <Typography color="textPrimary" variant="body1">
                        {doctor.EmployerInformation}
                      </Typography>
                    </Box>
                  </TableCell>
                  <TableCell>{doctor.Address}</TableCell>
                  <TableCell>{doctor.Gender}</TableCell>
                  <TableCell>{doctor.BirthDate}</TableCell>
                  <TableCell>{doctor.Age}</TableCell>
                  <TableCell>{doctor.InsuranceProvider}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={doctors.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  doctors: PropTypes.array.isRequired
};

export default Results;
