import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from 'src/layouts/DashboardLayout';
import UsersListView from 'src/views/users/UsersListView';
import PatientsListView from 'src/views/patients/PatientsListView';
import DoctorsListView from 'src/views/doctors/DoctorsListView';
import DashboardView from 'src/views/reports/DashboardView';
import Account from 'src/views/users/AccountView';

const routes = [
  {
    path: '/',
    element: <DashboardLayout />,
    children: [
      { path: 'users', element: <UsersListView /> },
      { path: 'patients', element: <PatientsListView /> },
      { path: 'doctors', element: <DoctorsListView /> },
      { path: 'acc', element: <Account /> },
      { path: 'dashboard', element: <DashboardView /> },
      { path: '/', element: <Navigate to="/dashboard" /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
